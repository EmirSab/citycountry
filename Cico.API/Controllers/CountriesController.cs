﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cico.API.Data;
using Cico.API.Data.Models;
using System.Linq.Dynamic.Core;

namespace Cico.API.Controllers
{
    //2.1 Dodati CountriesController -> Startup.cs
    // 7. Dodati GetCountries () ->Country.cs
    // https://localhost:5001/api/countries/?pageIndex= 0&pageSize= 2
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        public CountriesController(ApplicationDbContext context)
        {
            _context = context;
        }
        #region 6
        [HttpGet]
        public async Task<ActionResult<ApiResult<CountryDTO>>> GetCountries(int pageIndex = 0, int pageSize = 10,
                                                                        string sortColumn = null, string sortOrder = null,
                                                                        string filterColumn = null, string filterQuery = null)
        {
            // 13.2 Dodati include da pokupi ci cities ->CountryDTO
            // dodati dto i i da doda city propterije
            return await ApiResult<CountryDTO>.CreateAsync(
_context.Countries
// ako ne koristimo dto nego drugi nacin mjesto velikog selecta dodati samo ovo .Select(c => new Country() i ostali dio propertija
.Select(c => new CountryDTO()
{
Id = c.Id,
Name = c.Name,
ISO2 = c.ISO2,
ISO3 = c.ISO3,
TotCities = c.Cities.Count
}),
pageIndex,
pageSize,
sortColumn,
sortOrder,
filterColumn,
filterQuery);
        }
        #endregion

        // GET: api/Countries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Country>> GetCountry(int id)
        {
            var country = await _context.Countries.FindAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            return country;
        }

        // PUT: api/Countries/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCountry(int id, Country country)
        {
            if (id != country.Id)
            {
                return BadRequest();
            }

            _context.Entry(country).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Countries
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Country>> PostCountry(Country country)
        {
            _context.Countries.Add(country);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCountry", new { id = country.Id }, country);
        }

        // DELETE: api/Countries/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Country>> DeleteCountry(int id)
        {
            var country = await _context.Countries.FindAsync(id);
            if (country == null)
            {
                return NotFound();
            }

            _context.Countries.Remove(country);
            await _context.SaveChangesAsync();

            return country;
        }

        private bool CountryExists(int id)
        {
            return _context.Countries.Any(e => e.Id == id);
        }

        #region 11.1 Dodati IsDupeField() ->country-edit.component.html
        [HttpPost]
        [Route("IsDupeField")]
        public bool IsDupeField(int countryId,string fieldName,string fieldValue)
        {
            // 11.1 moze se uraditi na dva nacina prvi je zakomentarisan drugi ce se koristiti
            // Default approach (using strongly-typed LAMBA expressions)
            //switch (fieldName)
            //{
            // case "name":
            // return _context.Countries.Any(c => c.Name == fieldValue);
            // case "iso2":
            // return _context.Countries.Any(c => c.ISO2 == fieldValue);
            // case "iso3":
            // return _context.Countries.Any(c => c.ISO3 == fieldValue);
            // default:
            // return false;
            //}
            // Alternative approach (using System.Linq.Dynamic.Core)
            return (ApiResult<Country>.IsValidProperty(fieldName, true)) ? _context.Countries.Any(String.Format("{0} == @0 && Id != @1", fieldName),
            fieldValue, countryId) : false;
        }
        #endregion
    }
}
