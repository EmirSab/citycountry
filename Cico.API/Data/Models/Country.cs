using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Cico.API.Data.Models
{
    // 1.1 -> City.cs
    public class Country
    {

        #region Constructor
        public Country()
        {
        }
        #endregion
        #region Properties
        /// <summary>
        /// The unique id and primary key for this Country
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Country name (in UTF8 format)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Country code (in ISO 3166-1 ALPHA-2 format)
        /// </summary>
        // 7.1 dodati propetije na iso2 i iso3 ->Country.ts
        [JsonPropertyName("iso2")]
        public string ISO2 { get; set; }
        /// <summary>
        /// Country code (in ISO 3166-1 ALPHA-3 format)
        /// </summary>
        [JsonPropertyName("iso3")]
        public string ISO3 { get; set; }
        #endregion

        // 1.3 Dodai City u country ->ApplicationDataContext 
        #region Navigation Properties
        /// <summary>
        /// A list containing all the cities related to this country.
        /// </summary>
        #region ako zelimo da izbacimo dto dodati ovaj dio koda
        /*
        #region Client-side properties
/// <summary>
/// The number of cities related to this country.
/// </summary>
[NotMapped]
public int TotCities
{
get
{
return (Cities != null)
? Cities.Count
: _TotCities;
}
set { _TotCities = value; }
}
private int _TotCities = 0;
#endregion
i json igronre za Cities
[JsonIgnore]
        */
        public virtual List<City> Cities { get; set; }
        #endregion
    }
}
#endregion