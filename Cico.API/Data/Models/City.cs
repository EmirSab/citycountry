using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cico.API.Data.Models
{
    // 1.1 Dodati City i Country modele ->
    public class City
    {

       #region Constructor
        public City()
        {
        }
        #endregion
        #region Properties
        /// <summary>
        /// The unique id and primary key for this City
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// City name (in UTF8 format)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// City name (in ASCII format)
        /// </summary>
        public string Name_ASCII { get; set; }
        /// <summary>
        /// City latitude
        /// </summary>
        // 1.7 Dodati anotacije na property ->SeedController
        // instalirati dotnet add package EPPlus
        [Column(TypeName = "decimal(7,4)")]
        public decimal Lat { get; set; }
        /// <summary>
        /// City longitude
        /// </summary>
        [Column(TypeName = "decimal(7,4)")]
        public decimal Lon { get; set; }
        #endregion
        /// <summary>
        /// Country Id (foreign key)
        /// </summary>

        // 1.3 Dodati Country ->Country.cs
        [ForeignKey("Country")]
        public int CountryId { get; set; }
        #region Navigation Properties
        /// <summary>
        /// The country related to this city.
        /// </summary>
        public virtual Country Country { get; set; }
        #endregion
    }
}