using Cico.API.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Cico.API.Data
{
    // 1.4 Dodati propertije u context ->appsettings.json ->Startup.cs
    public class ApplicationDbContext : DbContext
    {
        // 1.4 Dodati propetije u context ->appsettings.json
        #region Constructor
        public ApplicationDbContext() : base()
        {
        }
        public ApplicationDbContext(DbContextOptions options)
        : base(options)
        {
        }
        #endregion Constructor
        #region Methods
        protected override void OnModelCreating(ModelBuilder
        modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Map Entity names to DB Table names
            modelBuilder.Entity<City>().ToTable("Cities");
            modelBuilder.Entity<Country>().ToTable("Countries");
        }
        #endregion Methods
        #region Properties
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        #endregion Properties
    }
}