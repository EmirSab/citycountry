import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseService } from '../base.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({
    providedIn: 'root',
})
    // 15.1 Napraviti city.service.ts ->app.module.ts
export class CityService extends BaseService {
    baseUrl = environment.apiUrl;
    constructor(http: HttpClient) {
        super(http);
    }
    getData<ApiResult>(
        pageIndex: number,
        pageSize: number,
        sortColumn: string,
        sortOrder: string,
        filterColumn: string,
        filterQuery: string
    ): Observable<ApiResult> {
        var url = this.baseUrl + 'api/Cities';
        var params = new HttpParams()
            .set('pageIndex', pageIndex.toString())
            .set('pageSize', pageSize.toString())
            .set('sortColumn', sortColumn)
            .set('sortOrder', sortOrder);
        if (filterQuery) {
            params = params
                .set('filterColumn', filterColumn)
                .set('filterQuery', filterQuery);
        }
        return this.http.get<ApiResult>(url, { params });
    }
    get<City>(id): Observable<City> {
        var url = this.baseUrl + 'api/Cities/' + id;
        return this.http.get<City>(url);
    }
    put<City>(item): Observable<City> {
        var url = this.baseUrl + 'api/Cities/' + item.id;
        return this.http.put<City>(url, item);
    }
    post<City>(item): Observable<City> {
        var url = this.baseUrl + 'api/Cities/' + item.id;
        return this.http.post<City>(url, item);
    }
}
