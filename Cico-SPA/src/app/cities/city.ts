//#region 3. Napraviti folder cities i u njemu komponentu city dodati propertije ->cities.component.ts
export interface City {
    id: number;
    name: string;
    lat: number;
    lon: number;
    // 9.4 dodati id ->city-edit.component.ts
    countryId: number;
    // 14.2 dodati ime zemlje ->cities.component.ts
    countryName: string;
    }
//#endregion
