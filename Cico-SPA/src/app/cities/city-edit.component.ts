import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { stringify } from '@angular/compiler/src/util';
import { City } from './City';
import { environment } from '../../environments/environment';
import { Country } from '../countries/country';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseFormComponent } from '../base.form.component';

//#region 8.1 Dodati edit komponentu i logiku ->city-edit.component.html
@Component({
    selector: 'app-city-edit',
    templateUrl: './city-edit.component.html',
    styleUrls: ['./city-edit.component.css']
})
// 12.4 extendati base form komponentu ->city-edit.component.ts
export class CityEditComponent extends BaseFormComponent {
    baseUrl = environment.apiUrl;
    // the view title
    title: string;
    // the form model
    form: FormGroup;
    // the city object to edit or create
    city: City;
    // 9. dodati id za dodavanje i dodati ostalu logiku ->cities.component.html
    // the city object id, as fetched from the active route:
    // It's NULL when we're adding a new city,
    // and not NULL when we're editing an existing one.
    id?: number;

    // 9.5 dodati countries ->city-edit.component.html
    // the countries array for the select
    countries: Country[];
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private http: HttpClient) {
            super();
    }
    ngOnInit() {
        // 10.1 Dodati validators ->city-edit.component.html
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            //#region 13. Dodati validaciju za lat i lon ->city-edit.component.html
            lat: new FormControl('', [
                Validators.required,
                Validators.pattern('^[-]?[0-9]+(\.[0-9]{1,4})?$')
                ]),
                lon: new FormControl('', [
                Validators.required,
                Validators.pattern('^[-]?[0-9]+(\.[0-9]{1,4})?$')
                ]),
            //#endregion
            // lat: new FormControl('', Validators.required),
            // lon: new FormControl('', Validators.required),
            // 9.5
            countryId: new FormControl('', Validators.required)
        }, null, this.isDupeCity());
        this.loadData();
    }
    //#region 10.3 dodati metod ->CitiesController.cs
    isDupeCity(): AsyncValidatorFn {
        return (control: AbstractControl): Observable<{
            [key: string]: any
        } | null> => {
            var city = <City>{};
            city.id = (this.id) ? this.id : 0;
            city.name = this.form.get('name').value;
            city.lat = +this.form.get('lat').value;
            city.lon = +this.form.get('lon').value;
            city.countryId = +this.form.get('countryId').value;
            var url = this.baseUrl + 'api/cities/IsDupeCity';
            return this.http.post<boolean>(url, city).pipe(map(result => {
                return (result ? { isDupeCity: true } : null);
            }));
        }
    }
    //#endregion
    loadData() {
        // 9.5
        // load countries
        this.loadCountries();
        //#region 9
        // retrieve the ID from the 'id'
        this.id = +this.activatedRoute.snapshot.paramMap.get('id');
        if (this.id) {
            // EDIT MODE
            // fetch the city from the server
            var url = this.baseUrl + 'api/cities/' + this.id;
            this.http.get<City>(url).subscribe(result => {
                this.city = result;
                this.title = 'Edit - ' + this.city.name;
                // update the form with the city value
                this.form.patchValue(this.city);
            }, error => console.error(error));
        }
        else {
            // ADD NEW MODE
            this.title = 'Create a new City';
        }
        //#endregion
    }
    //#region 9.5
    loadCountries() {
        // fetch all the countries from the server
        var url = this.baseUrl + 'api/countries';
        var params = new HttpParams()
            .set('pageSize', '9999')
            .set('sortColumn', 'name');
        this.http.get<any>(url, { params }).subscribe(result => {
            this.countries = result.data;
        }, error => console.error(error));
    }
    //#endregion
    onSubmit() {
        // 9
        var city = (this.id) ? this.city : <City>{};
        //var city = this.city;
        city.name = this.form.get('name').value;
        city.lat = +this.form.get('lat').value;
        city.lon = +this.form.get('lon').value;
        // 9.5
        city.countryId = +this.form.get('countryId').value;
        // 9
        if (this.id) {
            // EDIT mode
            var url = this.baseUrl + 'api/cities/' + this.city.id;
            this.http
                .put<City>(url, city)
                .subscribe(result => {
                    console.log('City ' + city.id + ' has been updated.');
                    // go back to cities view
                    this.router.navigate(['/cities']);
                }, error => console.log(error));
        }
        else {
            // ADD NEW mode
            var url = this.baseUrl + 'api/cities';
            this.http
                .post<City>(url, city)
                .subscribe(result => {
                    console.log('City ' + result.id + ' has been created.');
                    // go back to cities view
                    this.router.navigate(['/cities']);
                }, error => console.log(error));
        }
    }

    //#region 12 Dodati kratice za validaciju forme ->
    // retrieve a FormControl
    /*getControl(name: string) {
        return this.form.get(name);
    }
    // returns TRUE if the FormControl is valid
    isValid(name: string) {
        var e = this.getControl(name);
        return e && e.valid;
    }
    // returns TRUE if the FormControl has been changed
    isChanged(name: string) {
        var e = this.getControl(name);
        return e && (e.dirty || e.touched);
    }
    // returns TRUE if the FormControl is raising an error,
    // i.e. an invalid state after user changes
    hasError(name: string) {
        var e = this.getControl(name);
        return e && (e.dirty || e.touched) && e.invalid;
    }*/
    //#endregion
}

//#endregion
