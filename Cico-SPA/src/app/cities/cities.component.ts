//#region 3.1 Dodati logiku za cities ->environment.ts ->cities.component.html
import { Component, ViewChild } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { City } from './city';
import { environment } from '../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

// 4.1 Dodati mogul za paginaciju i table data source kao i viewchild ->cities.component.html
@Component({
    selector: 'app-cities',
    templateUrl: './cities.component.html',
    styleUrls: ['./cities.component.css']
})
export class CitiesComponent {
    baseUrl = environment.apiUrl;
    // 14.3 Dodati kolonu za ime zemlje ->
    public displayedColumns: string[] = ['id', 'name', 'lat', 'lon', 'countryName'];
    public cities: MatTableDataSource<City>;
    defaultPageIndex: number = 0;
    defaultPageSize: number = 10;
    public defaultSortColumn: string = 'name';
    public defaultSortOrder: string = 'asc';
    defaultFilterColumn: string = 'name';
    filterQuery: string = null;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(
        private http: HttpClient) {
    }

    ngOnInit(): void {
        // 5.3
        this.loadData(null);
    }
    // 6.2 Dodati filtere na front end i prepraviti loadData() ->cities.component.html
    //#region 5.3
    loadData(query: string = null) {
        var pageEvent = new PageEvent();
        pageEvent.pageIndex = this.defaultPageIndex;
        pageEvent.pageSize = this.defaultPageSize;
        if (query) {
            this.filterQuery = query;
            console.log(this.filterQuery);
        }
        this.getData(pageEvent);
    }
    //#endregion
    //#region 4.6
    // tslint:disable-next-line: typedef
    getData(event: PageEvent) {
        var url = this.baseUrl + 'api/Cities';
        var params = new HttpParams()
            .set('pageIndex', event.pageIndex.toString())
            .set('pageSize', event.pageSize.toString())
            .set('sortColumn', (this.sort)
                ? this.sort.active
                : this.defaultSortColumn)
            .set('sortOrder', (this.sort)
                ? this.sort.direction
                : this.defaultSortOrder);
        if (this.filterQuery) {
            params = params
                .set('filterColumn', this.defaultFilterColumn)
                .set('filterQuery', this.filterQuery);
            console.log(params);
        }
        /*
        .set('pageIndex', event.pageIndex.toString())
            .set('pageSize', event.pageSize.toString())
            .set('sortColumn', (this.sort)
                ? this.sort.active
                : this.defaultSortColumn)
            .set('sortOrder', (this.sort)
                ? this.sort.direction
                : this.defaultSortOrder);
                console.log(event.pageIndex);
        */
        this.http.get<any>(url, { params })
            .subscribe(result => {
                this.paginator.length = result.totalCount;
                this.paginator.pageIndex = result.pageIndex;
                this.paginator.pageSize = result.pageSize;
                this.cities = new MatTableDataSource<City>(result.data);
                console.log(this.paginator.length);
                console.log(this.paginator.pageIndex);
                console.log(this.paginator.pageSize);
                console.log(this.cities);
            }, error => console.error(error));
    }
    //#endregion
}
//#endregion
