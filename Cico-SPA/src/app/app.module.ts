import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CitiesComponent } from './cities/cities.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AngularMaterialModule } from './angular-material.module';
import { CountriesComponent } from './countries/countries.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CityEditComponent } from './cities/city-edit.component';
import { CountryEditComponent } from './countries/country-edit.component';
import { BaseFormComponent } from './base.form.component';


// 3.5 Dodati cities komponentu ->app.component.html ->app.module.ts
// Dodati httpClientModule -> instalirati angular material ng add @angular/material i dodati ga ->angular-material.module
 // 3.8 Dodati komponentu angular-material.module ->cities.component.ts
 // 7.6 dodati u app.module.ts ->home.html
 // 8. Dodati fromscontrol i reactive forms module ->city-edit.component.ts
 // 8.4 dodati city edit komponentu i rutu  ->cities.component.html
 // 9.3 dodati rutu ->city.ts
 // 11.4 Dodati country -edit komponentu i rute ->countries.component.html
 // 12.3 Dodati base form komponentu ->city-edit.component.ts
@NgModule({
  declarations: [
    AppComponent,
    CitiesComponent,
    HomeComponent,
    BaseFormComponent,
    CountriesComponent,
    CityEditComponent,
    CountryEditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatSliderModule,
    MatToolbarModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'cities', component: CitiesComponent },
      { path: 'city/:id', component: CityEditComponent },
      { path: 'city', component: CityEditComponent },
      { path: 'countries', component: CountriesComponent },
      // 11.4
      { path: 'country/:id', component: CountryEditComponent },
      { path: 'country', component: CountryEditComponent }
    ]),
    BrowserAnimationsModule,
    AngularMaterialModule, ReactiveFormsModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
