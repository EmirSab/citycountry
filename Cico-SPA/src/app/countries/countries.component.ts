import { Component, Inject, ViewChild } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Country } from './country';
import { environment } from '../../environments/environment';
@Component({
    selector: 'app-countries',
    templateUrl: './countries.component.html',
    styleUrls: ['./countries.component.css']
    })
// 7.3 dodati logiku za countries ->countries.component.html
export class CountriesComponent {
    baseUrl = environment.apiUrl;
    // 13.5 dodati kolonu za total city ->countries.component.html
    public displayedColumns: string[] = ['id', 'name', 'iso2', 'iso3', 'totCities'];
    public countries: MatTableDataSource<Country>;
    defaultPageIndex: number = 0;
    defaultPageSize: number = 10;
    public defaultSortColumn: string = 'name';
    public defaultSortOrder: string = 'asc';
    defaultFilterColumn: string = 'name';
    filterQuery: string = null;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    constructor(
        private http: HttpClient,
        ) {
    }
    ngOnInit() {
        this.loadData(null);
    }
    loadData(query: string = null) {
        var pageEvent = new PageEvent();
        pageEvent.pageIndex = this.defaultPageIndex;
        pageEvent.pageSize = this.defaultPageSize;
        if (query) {
            this.filterQuery = query;
        }
        this.getData(pageEvent);
    }
    getData(event: PageEvent) {
        var url = this.baseUrl + 'api/Countries';
        var params = new HttpParams()
            .set('pageIndex', event.pageIndex.toString())
            .set('pageSize', event.pageSize.toString())
            .set('sortColumn', (this.sort)
                ? this.sort.active
                : this.defaultSortColumn)
            .set('sortOrder', (this.sort)
                ? this.sort.direction
                : this.defaultSortOrder);
                console.log(event.pageIndex);
        if (this.filterQuery) {
            params = params
                .set('filterColumn', this.defaultFilterColumn)
                .set('filterQuery', this.filterQuery);
                console.log('Emir');
        }
        this.http.get<any>(url, { params })
            .subscribe(result => {
                this.paginator.length = result.totalCount;
                this.paginator.pageIndex = result.pageIndex;
                this.paginator.pageSize = result.pageSize;
                this.countries = new MatTableDataSource<Country>(result.data);
                console.log('Emir');
            }, error => console.error(error));
    }
}
