//#region 7.2 Dodati u folderu country komponentu country kao i interface i dodati propertije ->countries.component.ts
export interface Country {
    id: number;
    name: string;
    iso2: string;
    iso3: string;
    // 13.4 dodati property za cities ->countries.component.ts
    totCities: number;
    }
//#endregion
