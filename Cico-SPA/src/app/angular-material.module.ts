//#region 3.7 Dodati koponentu i logiku za tabele ->app.module.ts
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
// 4. Dodati modul za paginaciju ->cities.component.ts
// 5.2 Dodati sort module ->cities.component.ts
// 10 Dodati select module ->city-edit.html
@NgModule({
    imports: [
        MatTableModule,
        MatPaginatorModule, MatSortModule, MatFormFieldModule, MatInputModule, MatSelectModule
    ],
    exports: [
        MatTableModule,
        MatPaginatorModule, MatSortModule, MatFormFieldModule, MatInputModule, MatSelectModule
    ]
})
export class AngularMaterialModule { }
//#endregion
